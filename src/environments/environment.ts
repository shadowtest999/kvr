// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAHJ5mTUkmux3i1F_ijt6gqNYJcdIWFIEs",
    authDomain: "kvr-bill.firebaseapp.com",
    projectId: "kvr-bill",
    storageBucket: "kvr-bill.appspot.com",
    messagingSenderId: "1017932477462",
    appId: "1:1017932477462:web:30ec70c5cdf6a526df7137"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
