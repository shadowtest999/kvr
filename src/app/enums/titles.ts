export enum Titles {
    'name'  = 'Name',
    'addr'  = 'Address',
    'item'  = 'Item',
    'count' = 'Count',
    'kgs'   = 'Kgs',
    'gms'   = 'Gms',
    'rate'  = 'Rate' 
}
