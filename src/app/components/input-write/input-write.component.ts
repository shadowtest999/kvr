import { Component, Input, OnInit, ViewChild, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { SignaturePad } from 'angular2-signaturepad';

@Component({
  selector: 'app-input-write',
  templateUrl: './input-write.component.html',
  styleUrls: ['./input-write.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputWriteComponent),
      multi: true
    }
  ]
})
export class InputWriteComponent implements OnInit, ControlValueAccessor {

  _signature = '';
  isDrawing = false;

  
  onChange: any = () => { };
  onTouched: any = () => { };

  @Input() title:string = '';
  
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  
  // Check out https://github.com/szimek/signature_pad
  private signaturePadOptions: Object = { 
    'minWidth': 2,
    'canvasWidth': window.innerWidth,
    'canvasHeight': 60,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };

  constructor(private modalController:ModalController) { }

  ngOnInit() {}

  get signature() {
    return this._signature;
  }

  set signature(val) {
    this._signature = val;
    console.log(this.signature);
    this.onChange(this._signature);
  }

  drawComplete() {
    this.isDrawing = false;
    this.signature = this.signaturePad.toDataURL();
  }
 
  drawStart() {
    console.log('started');
    this.isDrawing = true;
  }
 
  savePad() {
    let item = this.signaturePad.toDataURL();
    // this.signaturePad.clear();
    return item;
  }
 
  clearPad() {
    this.signaturePad.clear();
    this.signature = '';
  }


  writeValue(value: any) {
    if (value !== undefined) {
      this._signature = value;
    }
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

}
