import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsPageRoutingModule } from './details-routing.module';

import { DetailsPage } from './details.page';
import { SignaturePadModule } from 'angular2-signaturepad';
import { InputWriteComponent } from '../components/input-write/input-write.component';

import { HttpClientModule } from '@angular/common/http';
import { FileOpener } from '@ionic-native/file-opener/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DetailsPageRoutingModule,
    SignaturePadModule,
    HttpClientModule
  ],
  declarations: [DetailsPage, InputWriteComponent]
})
export class DetailsPageModule {}
