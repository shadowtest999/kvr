import { Component, OnInit } from '@angular/core';
import { InputWriteComponent } from '../components/input-write/input-write.component';

import { FormControl, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Titles } from '../enums/titles';
import { BillService } from '../services/common/bill.service';
import { BillRepoService } from '../services//db/bill-repo.service';

import { createWorker } from 'tesseract.js';
import * as Tesseract from 'tesseract.js';

import { Filesystem } from '@capacitor/filesystem';
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
// const { Filesyste } 

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss']
})
export class DetailsPage implements OnInit {

  public billForm: FormGroup;
  public details:any = [];
  public titles = Titles;
  public worker:Tesseract.Worker;
  public progress:number = 0;
  private isReady = false;
  
  constructor(private fb: FormBuilder,
              private billService: BillService,
              private db: BillRepoService) { 
    
    this.loadWorker();
    this.billForm = this.fb.group({
      details: this.fb.array([ this.buildDetails() ])
    });
  }

  ngOnInit() {
  }

  name = new FormControl('');

  async saveDetails() {

    let res = this.billForm.get('details').value;
    await Promise.all( res.map( async(order) => {
      order.item =  await this.recognize(order.item);
    }));

    console.log(res);

    this.billService.setDetails(this.billForm.get('details').value);
    
    
    console.log(this.billForm.value);
    console.log(this.billForm.get('details').value);
  }

  buildDetails() {
    return this.fb.group({
      item: '',
      count: '',
      kgs: '',
      gms: '',
      rate: ''
    })
  }

  addDetails(): void {
    this.details = this.billForm.get('details') as FormArray;
    this.details.push(this.buildDetails());
  }

  get detailsControls() {
    return this.billForm.get('details')['controls'];
  }

  removeDetails(i: number) {
    this.details.removeAt(i);
  }

  
  async loadWorker()  {
    this.worker = createWorker({
      logger:progress => {
        if(progress.status == 'recognizing text') {
          this.progress = parseInt(''+ progress.progress * 100);
        }
      }
    });
    await this.worker.load();
    await this.worker.loadLanguage('eng')
    await this.worker.initialize('eng')
    this.isReady = true;
  }

  async recognize(img) {
    const result = await this.worker.recognize(img);
    return result.data.text;
  }

  createPdf() {

    let details = [];
    const docDefinition = {
      content: [
        { text: 'Name: ', style: 'subheader' },
		    { text: 'Address: opp. Vengi telephone exchange, venki sadan, Eluru, 534001' },
        { text: 'Date: ' },
        {
          style: 'tableExample',
          table: {
            widths: [200, 50, 50, 50, 50, 70],
            body: [
              ['Items', 'Count', 'Kgs', 'Gms', 'Rate', 'Total'],
              ['fixed-width cells have exactly the specified width', {text: 'nothing interesting here', italics: true, color: 'gray'}, {text: 'nothing interesting here', italics: true, color: 'gray'}, {text: 'nothing interesting here', italics: true, color: 'gray'}, 800, 900],
              [ 'Grand Total', '', '', '', '', {text:900 }],
            ]
          }
        }
      ]
    };
  }



}
