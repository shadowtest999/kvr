import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, CollectionReference } from 'angularfire2/firestore';
import { Details } from '../../interfaces/details';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Bill } from '../../interfaces/bill';

@Injectable({
  providedIn: 'root'
})
export class BillService {

  private bill:Bill = {
    name : '',
    address: '',
    date: '',
    details: [],
    total: 0
  }

  constructor(private db: AngularFirestore) {
    
  }

  setName(name: string, address: string, date:string) {
    this.bill.name = name;
    this.bill.address = address,
    this.bill.date = date;
  }

  getBill() {
    return this.bill;
  }

  setDetails(details:Details[]) {
    let amount = 0;
    let total = 0;

    details.map(item=> {
      item.amount = (item.kgs + (item.gms / 1000)) * item.rate;
      total += item.amount;
    });

    this.bill.details = details;
    this.bill.total = total;
  }
  
}
