import { Injectable } from '@angular/core';

import * as Tesseract from 'tesseract.js';

@Injectable({
  providedIn: 'root'
})
export class OcrService {

  private readonly tesseract;

  constructor() {
    console.log('Hello OcrProvider');
    /**
     * Create our tesseract instance.
     */
    this.tesseract = Tesseract.createWorker({
      workerPath: '/',
      langPath: '/path/to/lang/',
      corePath: '/path/to/core.js',
    });
  }

}
