import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, CollectionReference } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Bill } from '../../interfaces/bill';

@Injectable({
  providedIn: 'root'
})
export class BillRepoService {


  private billsCollection:AngularFirestoreCollection<Bill>;
  private bills:Observable<any>;

  constructor(private db: AngularFirestore) {
    this.billsCollection = this.db.collection<Bill>('bills');

    this.bills = this.billsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc['id'];
          return {id, data};
        })
      })
    )
  }

  getBills() {
    return this.bills;
  }

  getBill(id:string) {
    return this.billsCollection.doc<Bill>(id).valueChanges();
  }

  updateBill(bill: Bill, id: string) {
    return this.billsCollection.doc(id).update(bill);
  }

  addBill(bill: Bill) {
    return this.billsCollection.add(bill);
  }

  removeBill(id: string) {
    return this.billsCollection.doc(id).delete();
  }

}