import { Details } from "./details";

export interface Bill {
    name: string,
    address: string,
    date:string,
    details: Details[],
    total: number
}
