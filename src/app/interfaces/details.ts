export interface Details {
    variety: string,
    count: number,
    kgs: number,
    gms: number,
    rate: number,
    amount: number
}
