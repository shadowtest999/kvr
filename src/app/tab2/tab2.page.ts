import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { BillRepoService } from '../services/db/bill-repo.service';
import { Router, NavigationExtras } from '@angular/router';

import { Bill } from '../interfaces/bill';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public bills:any;

  constructor(private db:BillRepoService, 
              private router: Router) {
    db.getBills().subscribe(data=> {
      this.bills = data;      
    })
  }

  loadData(event) {
    setTimeout(() => {
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.bills.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  openBill(id) {
    alert(id);
    let navigationExtras: NavigationExtras = {
      state: {
        id: id
      }
    };
    this.router.navigate(['single'], navigationExtras);
  }

  

}
