import { Component, ViewChild, Input } from '@angular/core';
import { InputWriteComponent } from '../components/input-write/input-write.component';

import { createWorker } from 'tesseract.js';
import { BillService } from '../services/common/bill.service'
import { Titles } from './../enums/titles';

import { Router } from '@angular/router';


// const { createWorker, createScheduler } = require('tesseract.js');

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public worker: Tesseract.Worker;
  private val = '';
  public progress:number = 0;

  public data:any = {
    name: '',
    addr: '',
    date: ''
  }

  private isReady = false;

  public titles = Titles;
  
  @ViewChild('name') name: InputWriteComponent;
  @ViewChild('address') address: InputWriteComponent;

  constructor(private billService: BillService, 
              private router: Router ) {
      this.loadWorker();
  }

  async billSave() {
    let pngName = this.name.savePad();
    let pngAddr = this.address.savePad();

    this.data.name = await this.recognize(pngName);
    this.data.addr = await this.recognize(pngAddr);

    this.billService.setName(this.data.name, this.data.addr, this.data.date);
    this.router.navigate(['/details']);

    // let folderpath = File.externalRootDirectory;

    // let filename = "ourcodeworld.png";
    // let myBaseString = this.name.savePad();
    // // Split the base64 string in data and contentType
    // var block = myBaseString.split(";");
    // // Get the content type
    // var dataType = block[0].split(":")[1];// In this case "image/png"
    // // get the real base64 content of the file
    // var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

    // this.base64ToImageService.savebase64AsImageFile(folderpath,filename,realData,dataType);
  }

  async loadWorker()  {
    this.worker = createWorker({
      logger:progress => {
        if(progress.status == 'recognizing text') {
          this.progress = parseInt(''+ progress.progress * 100);
        }
      }
    });
    await this.worker.load();
    await this.worker.loadLanguage('eng')
    await this.worker.initialize('eng')
    this.isReady = true;
  }

  async recognize(img) {
    const result = await this.worker.recognize(img);
    return result.data.text;
  }

}
