import { Component, OnInit } from '@angular/core';
import { Titles } from '../enums/titles';
import { Bill } from '../interfaces/bill';
import { BillRepoService } from '../services/db/bill-repo.service';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-single',
  templateUrl: './single.page.html',
  styleUrls: ['./single.page.scss'],
})
export class SinglePage implements OnInit {

  bill: Bill;
  billId = '';
  public titles = Titles;

  constructor(private db: BillRepoService,
              private route: ActivatedRoute, 
              private router: Router
  ) {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.billId= this.router.getCurrentNavigation().extras.state.id;
      }
    });

  }

  ngOnInit() {
    this.db.getBill(this.billId).subscribe(data => {
      this.bill = data;
      console.log(this.bill);
    })
  }

}
